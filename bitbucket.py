#!/usr/bin/python
import os
from optparse import OptionParser

parser = OptionParser()

parser.add_option("-u", "--username", dest="username",
                  	help="bitbucket username")
parser.add_option("-p", "--password", dest="password",
					help="bitbucket password")
parser.add_option("-n", "--reponame", dest="reponame",
					help="repo name want to create")

parser.add_option("-t", "--scmtype", dest="scmtype", default='git',
					help="repo type")

parser.add_option("-m", "--commitmsg", dest="commitmsg", default='initial commit',
					help="repo commit message")

parser.add_option("-r", "--remote", dest="remote", default='bitbucket',
					help="repo remote name")


(options, args) = parser.parse_args()


os.system('curl -X POST -u %s:%s https://api.bitbucket.org/1.0/repositories/ -d name=%s -d scm=%s' % (options.username,options.password,options.reponame,options.scmtype))

os.system('git init .')
os.system('git add .')
os.system('git commit -a -s -m "%s"' % (options.commitmsg))
os.system('git remote add %s git@bitbucket.org:%s/%s.git' %(options.remote,options.username,options.reponame))
os.system('git push -u %s master' % (options.remote))